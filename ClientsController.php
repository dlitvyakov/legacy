<?php

class User_ClientsController extends BaseController
{
    public function getDetails($id)
    {
        /** @var Client $client */
        $client = Client::where('id', '=', $id)->where('company', '=', Company::current()->id)->firstOrFail();
        $ticketsQuery = $client->tickets();
        Ticket::checkUserAccess($ticketsQuery, Auth::user()->user());

        if(Input::has('time')){
            if ( Input::get('time')=='asc'){
                $ticketsQuery->orderBy('published', 'asc');
            }
            else{
                $ticketsQuery->orderBy('published', 'desc');
            }

        }
        else if(Input::has('agent')){
            if(Input::get('agent')=='asc'){
                $ticketsQuery->orderBy('assignee', 'asc');
            }
            else{
                $ticketsQuery->orderBy('assignee', 'desc');
            }
        }
        else {
            $ticketsQuery->orderByRaw(\DB::raw("FIELD(status_id, 2, 3, 1, 6, 4) ASC"))->orderBy('updated_at', 'desc');
        }

        return View::make('user.clients.details', [
            'client' => $client,
            'tickets' => $ticketsQuery->paginate(),
        ]);
    }

    public function postEdit($id = null)
    {
        $rules = [
            'name' => 'required',
            'avatar' => 'image',
            'spamer' => 'boolean',
            'vip' => 'boolean',
        ];
        if (Input::get('client_company_id') != '_new_') {
            $rules['client_company_id'] = 'exists:client_companies,id,company_id,' . Company::current()->id;
        } else {
            $rules['client_company_name'] = 'required';
        }

        for ($i = 0; $i < count(Input::get('phones.phone')); $i++) {
            if (count(Input::get('phones.phone')) == 1) {
                $rules['phones.phone.' . $i] = 'phone';
                $rules['phones.type.' . $i] = 'required_with:phones.phone.' . $i . '|in:' . implode(',', ClientPhone::getTypeList());
            } else {
                $rules['phones.phone.' . $i] = 'required|phone';
                $rules['phones.type.' . $i] = 'required|in:' . implode(',', ClientPhone::getTypeList());
            }
        }
        for ($i = 0; $i < count(Input::get('emails.email')); $i++) {
            if (count(Input::get('emails.email')) == 1) {
                $rules['emails.email.' . $i] = 'email|client_email_unique:' . Company::current()->id . ',' . $id;
            } else {
                $rules['emails.email.' . $i] = 'required|email|client_email_unique:' . Company::current()->id . ',' . $id;
            }
        }
        for ($i = 0; $i < count(Input::get('sites.url')); $i++) {
            if (count(Input::get('sites.url')) == 1) {
                $rules['sites.url.' . $i] = 'url';
            } else {
                $rules['sites.url.' . $i] = 'required|url';
            }
        }
        for ($i = 0; $i < count(Input::get('social_networks.url')); $i++) {
            if (count(Input::get('social_networks.url')) == 1) {
                $rules['social_networks.url.' . $i] = 'url';
                $rules['social_networks.type.' . $i] = 'required_with:social_networks.url.' . $i . '|in:' . implode(',', ClientSocialNetwork::getTypeList());
            } else {
                $rules['social_networks.url.' . $i] = 'required|url';
                $rules['social_networks.type.' . $i] = 'required|in:' . implode(',', ClientSocialNetwork::getTypeList());
            }
        }
        for ($i = 0; $i < count(Input::get('messengers.identity')); $i++) {
            if (count(Input::get('messengers.identity')) == 1) {
                $rules['messengers.type.' . $i] = 'required_with:messengers.url.' . $i . '|in:' . implode(',', ClientMessenger::getTypeList());
            } else {
                $rules['messengers.identity.' . $i] = 'required';
                $rules['messengers.type.' . $i] = 'required|in:' . implode(',', ClientMessenger::getTypeList());
            }
        }
        for ($i = 0; $i < count(Input::get('addresses.country')); $i++) {
            if (count(Input::get('addresses.country')) == 1) {
                $rules['addresses.country.' . $i] = 'required_with:addresses.city.' . $i . ',addresses.address.' . $i . '';
                $rules['addresses.city.' . $i] = 'required_with:addresses.country.' . $i . ',addresses.address.' . $i . '';
                $rules['addresses.address.' . $i] = 'required_with:addresses.country.' . $i . ',addresses.city.' . $i . '';
                $rules['addresses.type.' . $i] = 'required_with:addresses.country.' . $i . ',addresses.city.' . $i . ',addresses.address.' . $i . '|in:' . implode(',', ClientAddress::getTypeList());
            } else {
                $rules['addresses.country.' . $i] = 'required';
                $rules['addresses.city.' . $i] = 'required';
                $rules['addresses.address.' . $i] = 'required';
                $rules['addresses.type.' . $i] = 'required|in:' . implode(',', ClientAddress::getTypeList());

            }
        }
        $validator = Validator::make(Input::only(array_keys($rules)) + ['avatar' => Input::file('avatar')], $rules);
        if ($validator->fails()) {
            return Response::json(['errors' => $validator->messages()->toArray()]);
        }
        try {
            DB::beginTransaction();
            $client = Client::firstOrNew(['id' => $id, 'company_id' => Company::current()->id]);
            $client->fill(Input::only('name', 'position', 'note'));

            if (Input::get('spamer')) {
                $client->spamer = true;
            }
			elseif($client->spamer) {
				if (Input::get('emails.email') ) {

					for ($i = 0; $i < count(Input::get('emails.email')); $i++) {
						$blackListDeleteQuery = BlackWhiteList::where('company_id', '=', Company::current()->id)
							->where('type', '=', BlackWhiteList::TYPE_BLACK)
							->where('email', '=', Input::get('emails.email.' . $i) );
						$blackListDeleteQuery->delete();					
					}
				}
				$client->spammer = false;
			}
			$client->vip = false;
            if (Input::get('vip')) {
                $client->vip = true;
            }

            if (!Input::get('client_company_id')) {
                $client->client_company_id = null;
            } elseif (Input::get('client_company_id') == '_new_') {
                $clientCompany = ClientCompany::create([
                    'name' => Input::get('client_company_name'),
                    'company_id' => Company::current()->id,
                ]);
                $client->client_company_id = $clientCompany->id;
            } else {
                $client->client_company_id = Input::get('client_company_id');
            }
            $client->save();
            ClientPhone::where('client_id', '=', $client->id)->delete();
            if (Input::get('phones.phone.0') || count(Input::get('phones.phone')) > 1) {
                for ($i = 0; $i < count(Input::get('phones.phone')); $i++) {
                    ClientPhone::firstOrCreate(['phone' => Input::get('phones.phone.' . $i), 'type' => Input::get('phones.type.' . $i), 'client_id' => $client->id]);
                }
            }
            ClientEmail::where('client_id', '=', $client->id)->delete();
            if (Input::get('emails.email.0') || count(Input::get('emails.email')) > 1) {
                for ($i = 0; $i < count(Input::get('emails.email')); $i++) {
                    ClientEmail::firstOrCreate(['email' => Input::get('emails.email.' . $i), 'client_id' => $client->id]);
                }
            }
            ClientSite::where('client_id', '=', $client->id)->delete();
            if (Input::get('sites.url.0') || count(Input::get('sites.url')) > 1) {
                for ($i = 0; $i < count(Input::get('sites.url')); $i++) {
                    ClientSite::firstOrCreate(['url' => Input::get('sites.url.' . $i), 'client_id' => $client->id]);
                }
            }
            ClientSocialNetwork::where('client_id', '=', $client->id)->delete();
            if (Input::get('social_networks.url.0') || count(Input::get('social_networks.url')) > 1) {
                for ($i = 0; $i < count(Input::get('social_networks.url')); $i++) {
                    ClientSocialNetwork::firstOrCreate(['url' => Input::get('social_networks.url.' . $i), 'type' => Input::get('social_networks.type.' . $i), 'client_id' => $client->id]);
                }
            }
            ClientMessenger::where('client_id', '=', $client->id)->delete();
            if (Input::get('messengers.identity.0') || count(Input::get('messengers.identity')) > 1) {
                for ($i = 0; $i < count(Input::get('messengers.identity')); $i++) {
                    ClientMessenger::firstOrCreate(['identity' => Input::get('messengers.identity.' . $i), 'type' => Input::get('messengers.type.' . $i), 'client_id' => $client->id]);
                }
            }
            ClientAddress::where('client_id', '=', $client->id)->delete();
            if (Input::get('addresses.country.0') || count(Input::get('addresses.country')) > 1) {
                for ($i = 0; $i < count(Input::get('addresses.country')); $i++) {
                    ClientAddress::firstOrCreate([
                        'country' => Input::get('addresses.country.' . $i),
                        'city' => Input::get('addresses.city.' . $i),
                        'address' => Input::get('addresses.address.' . $i),
                        'type' => Input::get('addresses.type.' . $i),
                        'client_id' => $client->id
                    ]);
                }
            }


            $additionalIds = array_keys(Input::get('additional_ids'));
            $arAdditionalIds = Input::get('additional_ids');
            if (count($arAdditionalIds)) {
                ClientAdditionalId::whereNotIn('id', $additionalIds)->where('client_id', $client->id)->delete();
                foreach ($arAdditionalIds as $id => $value) {
                    if (ClientAdditionalId::where('id', $id)->where('client_id', $client->id)->exists()) {
                        if ($value) {
                            ClientAdditionalId::where('id', $id)->where('client_id', $client->id)->update([
                                'value' => $value
                            ]);
                        } else {
                            ClientAdditionalId::where('id', $id)->where('client_id', $client->id)->delete();
                        }
                    } elseif ($value) {
                        ClientAdditionalId::create([
                            'client_id' => $client->id,
                            'value' => $value,
                        ]);
                    }

                }
            } else {
                ClientAdditionalId::where('client_io', $client->id)->delete();
            }
            ClientAddress::where('client_id', '=', $client->id)->delete();
            if (Input::get('addresses.country.0') || count(Input::get('addresses.country')) > 1) {
                for ($i = 0; $i < count(Input::get('addresses.country')); $i++) {
                    ClientAddress::firstOrCreate([
                        'country' => Input::get('addresses.country.' . $i),
                        'city' => Input::get('addresses.city.' . $i),
                        'address' => Input::get('addresses.address.' . $i),
                        'type' => Input::get('addresses.type.' . $i),
                        'client_id' => $client->id
                    ]);
                }
            }

            if (Input::hasFile('avatar')) {
                $client->setAvatar(Input::file('avatar'));
            }
            $client->save();
            if ($client->spammer) {
                Ticket::where('client_id', '=', $client->id)
                    ->update([
                        'status_id' => TicketStatus::getByKey(TicketStatus::SYSTEM_SPAM)->id,
                    ]);
				if (Input::get('emails.email') ) {
					for ($i = 0; $i < count(Input::get('emails.email')); $i++) {
						BlackWhiteList::firstOrCreate([
							'email' =>Input::get('emails.email.' . $i),
							'type' => BlackWhiteList::TYPE_BLACK,
							'company_id' => Company::current()->id,
						]);						
					}
				}
            }
			if ($client->vip) {
				if (Input::get('emails.email') ) {
					for ($i = 0; $i < count(Input::get('emails.email')); $i++) {
						BlackWhiteList::firstOrCreate([
							'email' =>Input::get('emails.email.' . $i),
							'type' => BlackWhiteList::TYPE_WHITE,
							'company_id' => Company::current()->id,
						]);						
					}
				}
			}
            DB::commit();
        } catch (Exception $e) {
            Log::error($e);
            DB::rollBack();
        }

        if (CompanyIntegration::boolCheck(Integration::TYPE_FULLCONTACT,$client->company_id)) {

            \UseDesk\Fullcontact\Fullcontact::socialsByEmail($client->id);

        }

        return Response::json(['redirect' => URL::route('user.clients.get_details', $client->id)]);
    }

    public function getDelete($id)
    {
        Client::where('id', '=', $id)->where('company_id', '=', Company::current())->delete();
        return Redirect::back();
    }

    public function getDeleteAvatar($id)
    {
        /** @var Client $client */
        $client = Client::where('id', '=', $id)->where('company_id', '=', Company::current()->id)->firstOrFail();
        $client->avatar = null;
        $client->save();
        return Redirect::back();
    }
    

}
