<?php

class User_SearchController extends BaseController
{

    public function getIndex()
    {

        if ($search = Input::get('search')) {
            $searchFilter = '%' . $search . '%';
            $ticketsQuery = Ticket::select('tickets.id', 'tickets.subject', 'tickets.assignee_id', 'tickets.last_updated_at',
                'tickets.client_id', 'clients.name as client_name', 'users.name as user_name')
                ->leftJoin('clients', 'clients.id', '=', 'tickets.client_id')
                ->leftJoin('users', 'users.id', '=', 'tickets.assignee_id')
                ->leftJoin('ticket_comments', 'ticket_comments.ticket_id', '=', 'tickets.id')
                ->where('tickets.company_id', '=', Company::current()->id)
                ->where(function ($q) use ($searchFilter, $search) {
                    $q->where('tickets.subject', 'LIKE', $searchFilter)
                        ->orWhere('tickets.id', '=', trim($searchFilter, "%"));
                    $q->orWhereRaw('MATCH(ticket_comments.message) AGAINST(? IN BOOLEAN MODE)', ['"'.$search.'"']);
                })
                ->groupBy('tickets.id')
                ->orderBy('tickets.last_updated_at', 'desc');
            $clientQuery = Client::select('clients.id', 'clients.name', 'clients.vip', 'clients.avatar',
                DB::raw("CONCAT(client_companies.id, '+-+', client_companies.name) as client_company"),
                DB::raw("GROUP_CONCAT(DISTINCT CONCAT(client_phones.type, '+-+', client_phones.phone) SEPARATOR '||') as client_phones"),
                DB::raw("GROUP_CONCAT(DISTINCT CONCAT(client_emails.id, '+-+', client_emails.email) SEPARATOR '||') as client_emails"),
                DB::raw("GROUP_CONCAT(DISTINCT client_sites.url SEPARATOR '||') as client_sites"),
                DB::raw("GROUP_CONCAT(DISTINCT CONCAT(client_social_networks.type, '+-+', client_social_networks.url) SEPARATOR '||') as client_social_networks"),
                DB::raw("GROUP_CONCAT(DISTINCT CONCAT(client_messengers.type, '+-+', client_messengers.identity) SEPARATOR ', ') as client_messengers"),
                DB::raw("GROUP_CONCAT(DISTINCT CONCAT(client_addresses.type, '+-+', client_addresses.country, ', ',client_addresses.city, ', ', client_addresses.address) SEPARATOR '||') as client_addresses"))
                ->leftJoin('client_phones', 'client_phones.client_id', '=', 'clients.id')
                ->leftJoin('client_emails', 'client_emails.client_id', '=', 'clients.id')
                ->leftJoin('client_sites', 'client_sites.client_id', '=', 'clients.id')
                ->leftJoin('client_messengers', 'client_messengers.client_id', '=', 'clients.id')
                ->leftJoin('client_social_networks', 'client_social_networks.client_id', '=', 'clients.id')
                ->leftJoin('client_addresses', 'client_addresses.client_id', '=', 'clients.client_company_id')
                ->leftJoin('client_companies', 'client_companies.id', '=', 'clients.id')
                ->where('clients.company_id', '=', Company::current()->id)
                ->where(function ($emailsQuery) use ($searchFilter) {
                    $emailsQuery->where('clients.name', 'LIKE', $searchFilter)
                        ->orWhere('client_phones.phone', 'LIKE', $searchFilter)
                        ->orWhere('client_emails.email', 'LIKE', $searchFilter)
                        ->orWhere('client_social_networks.url', 'LIKE', $searchFilter);
                })->groupBy('clients.id');
            $clientCompanyQuery = ClientCompany::where('company_id', '=', Company::current()->id)
                ->where('name', 'LIKE', $searchFilter);
            if (Auth::user()->user()->checkPermission(UserGroupPermission::PERMISSION_SETTINGS)) {
                $usersQuery = User::select('id', 'name', 'email', 'phone')
                    ->where('company_id', '=', Company::current()->id)
                    ->where(function ($emailsQuery) use ($searchFilter){
                        $emailsQuery->orWhere('name', 'LIKE', $searchFilter)
                            ->orWhere('email', 'LIKE', $searchFilter)
                            ->orWhere('phone', 'LIKE', $searchFilter);
                    })->where('status',true);
            }
        }
        return View::make('user.search.index', [
            'tickets' => isset($ticketsQuery) ? $ticketsQuery->limit(20)->get() : null,
            'clients' => isset($clientQuery) ? $clientQuery->limit(20)->get() : null,
            'clientCompanies' => isset($clientCompanyQuery) ? $clientCompanyQuery->limit(20)->get() : null,
            'users' => isset($usersQuery) ? $usersQuery->limit(20)->get() : null,
        ]);
    }

    public function getDetails($type) {
        if (!in_array($type, ['tickets', 'clients', 'users', 'companies'])) throw new ExceptionHTTP(400);
        //if (!Input::has('search')) throw new ExceptionHTTP(400);
        $search = Input::get('search');
        $searchFilter = '%' . $search . '%';
        $data = [];
        if ($search) {
            switch ($type) {
                case 'tickets':
                    $data = Ticket::select('tickets.id', 'tickets.subject', 'tickets.assignee_id', 'tickets.last_updated_at',
                        'tickets.client_id', 'clients.name as client_name', 'users.name as user_name')
                        ->leftJoin('clients', 'clients.id', '=', 'tickets.client_id')
                        ->leftJoin('users', 'users.id', '=', 'tickets.assignee_id')
                        ->leftJoin('ticket_comments', 'ticket_comments.ticket_id', '=', 'tickets.id')
                        ->where('tickets.company_id', '=', Company::current()->id)
                        ->where(function ($q) use ($searchFilter, $search) {
                            $q->where('tickets.subject', 'LIKE', $searchFilter)
                                ->orWhere('tickets.id', '=', trim($searchFilter, "%"));
                            $q->orWhereRaw('MATCH(ticket_comments.message) AGAINST(? IN BOOLEAN MODE)', ['"'.$search.'"']);
                        })
                        ->groupBy('tickets.id')
                        ->orderBy('tickets.last_updated_at', 'desc')->paginate();
                    break;

                case 'clients':
                    $data =  Client::select('clients.id', 'clients.name', 'clients.vip', 'clients.avatar',
                        DB::raw("CONCAT(client_companies.id, '+-+', client_companies.name) as client_company"),
                        DB::raw("GROUP_CONCAT(DISTINCT CONCAT(client_phones.type, '+-+', client_phones.phone) SEPARATOR '||') as client_phones"),
                        DB::raw("GROUP_CONCAT(DISTINCT CONCAT(client_emails.id, '+-+', client_emails.email) SEPARATOR '||') as client_emails"),
                        DB::raw("GROUP_CONCAT(DISTINCT client_sites.url SEPARATOR '||') as client_sites"),
                        DB::raw("GROUP_CONCAT(DISTINCT CONCAT(client_social_networks.type, '+-+', client_social_networks.url) SEPARATOR '||') as client_social_networks"),
                        DB::raw("GROUP_CONCAT(DISTINCT CONCAT(client_messengers.type, '+-+', client_messengers.identity) SEPARATOR ', ') as client_messengers"),
                        DB::raw("GROUP_CONCAT(DISTINCT CONCAT(client_addresses.type, '+-+', client_addresses.country, ', ',client_addresses.city, ', ', client_addresses.address) SEPARATOR '||') as client_addresses"))
                        ->leftJoin('client_phones', 'client_phones.client_id', '=', 'clients.id')
                        ->leftJoin('client_emails', 'client_emails.client_id', '=', 'clients.id')
                        ->leftJoin('client_sites', 'client_sites.client_id', '=', 'clients.id')
                        ->leftJoin('client_messengers', 'client_messengers.client_id', '=', 'clients.id')
                        ->leftJoin('client_social_networks', 'client_social_networks.client_id', '=', 'clients.id')
                        ->leftJoin('client_addresses', 'client_addresses.client_id', '=', 'clients.client_company_id')
                        ->leftJoin('client_companies', 'client_companies.id', '=', 'clients.id')
                        ->where('clients.company_id', '=', Company::current()->id)
                        ->where(function ($emailsQuery) use ($searchFilter) {
                            $emailsQuery->where('clients.name', 'LIKE', $searchFilter)
                                ->orWhere('client_phones.phone', 'LIKE', $searchFilter)
                                ->orWhere('client_emails.email', 'LIKE', $searchFilter)
                                ->orWhere('client_social_networks.url', 'LIKE', $searchFilter);
                        })
                        ->groupBy('clients.id')
                        ->paginate();
                    break;

                case 'users':
                    if (Auth::user()->user()->checkPermission(UserGroupPermission::PERMISSION_SETTINGS)) {
                        $data = User::select('id', 'name', 'email', 'phone')
                            ->where('company_id', '=', Company::current()->id)
                            ->where(function ($emailsQuery) use ($searchFilter){
                                $emailsQuery->orWhere('name', 'LIKE', $searchFilter)
                                    ->orWhere('email', 'LIKE', $searchFilter)
                                    ->orWhere('phone', 'LIKE', $searchFilter);


                            })->where('status',true)->paginate();
                    } else {
                        $data = [];
                    }
                    break;

                case 'companies':
                    $data = ClientCompany::where('company_id', '=', Company::current()->id)
                        ->where('name', 'LIKE', $searchFilter)->orderBy('id', 'desc')->paginate();
                    break;

                default:
                    $data = [];
                    break;
            }
        }

        return View::make('user.search.details', [
            'data' => $data,
            'type' => $type,
            'links' => is_object($data) ? $data->appends(Input::all())->links() : ''
        ]);
    }
}